from flask import Flask, render_template, g 
import requests
import sqlite3 as sql
from DbPopulate import DbPopulate
import json

app = Flask(__name__)
listOfVideoData = []
displayStartRow = 0
displayEndRow = 30

class VideoData:
	def __init__(self, id, title, url, time, points, created_at, source, image): #add image parameter
		self.id = id
		self.title = title
		self.url = url
		self.time = time
		self.points = points
		self.created_at = created_at
		self.source = source
		self.image = image

@app.route('/')
def index():
	global listOfVideoData
	global displayStartRow
	global displayEndRow

	DbPopulate.populate_db()
	con = sql.connect("database.db")
	con.row_factory = sql.Row

	cur = con.cursor()
	cur.execute("select * from hackertv")
	rows = cur.fetchall();

	for row in rows:
		id = row['id']
		title = row['title']
		url = row['url']
		time = row['time']
		points = row['points']
		created_at = row['created_at']
		source = row['source']
		image = row['image']
		tempVideoData = VideoData(id, title, url, time, points, created_at, source, image)
		listOfVideoData.append(tempVideoData)
	
	videoDataToHTML = listOfVideoData[0: 30]
	return render_template("main.html",rows = videoDataToHTML)

@app.route('/more')
def more():
	global listOfVideoData
	global displayStartRow
	global displayEndRow

	displayStartRow += 30
	displayEndRow += 30
	videoDataToHTML = listOfVideoData[displayStartRow: displayEndRow]
	
	return render_template("main.html",rows = videoDataToHTML)	

@app.route('/tvml')
def tvml():
	con = sql.connect("database.db")
	con.row_factory = sql.Row
	db = con.cursor()

	rows = db.execute('''
		SELECT * from hackertv LIMIT 30 
		''').fetchall()
	con.close()

	return json.dumps( [dict(ix) for ix in rows] )

if __name__ == '__main__':
	app.run(host="0.0.0.0", debug=True, port=5000)	