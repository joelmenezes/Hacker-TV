import requests
import sqlite3 as sql
import os
from urlparse import urlparse, parse_qs

class DbPopulate:
	@classmethod
	def populate_db(cls):
		db = "database.db"
		if not os.path.isfile(db):
			conn = sql.connect(db)
			conn.execute(
				'CREATE TABLE hackertv(id TEXT, title TEXT, url TEXT, time TEXT, points TEXT, created_at TEXT, source TEXT, image TEXT, PRIMARY KEY(id))')
			conn.close()
			
		
		r = requests.get('http://hn.algolia.com/api/v1/search?query=[video]&tags=story&hitsPerPage=1000')
		r = r.json()
		with sql.connect(db) as con:
			for hit in r['hits']:

				sourceAndImage = getSourceAndImg(hit['url'])
				
				date = hit['created_at'].strip()
				date = date.split("T")

				cur = con.cursor()
				sqlQuery = "INSERT OR REPLACE INTO hackertv (id,title,url,time,points,created_at,source,image) VALUES (?,?,?,?,?,?,?,?)"
				cur.execute(sqlQuery,(hit['objectID'],hit['title'],hit['url'],hit['created_at_i'],hit['points'],date[0],sourceAndImage['source'],sourceAndImage['imgSrc']))
		
def getSourceAndImg(url):
	query = urlparse(url)

	imgSrc = ''
	source = query.netloc

	if query.hostname == 'youtu.be':
		 imgID = query.path[1:]
		 imgSrc = "http://img.youtube.com/vi/"+imgID+"/maxresdefault.jpg"
	if query.hostname in ('www.youtube.com', 'youtube.com'):
		if query.path == '/watch':
		    p = parse_qs(query.query)
		    imgID = p['v'][0]
		    imgSrc = "http://img.youtube.com/vi/"+imgID+"/maxresdefault.jpg"
		if query.path[:7] == '/embed/':
		    imgID = query.path.split('/')[2]
		    imgSrc = "http://img.youtube.com/vi/"+imgID+"/maxresdefault.jpg"
		if query.path[:3] == '/v/':
		    imgID = query.path.split('/')[2]
		    imgSrc = "http://img.youtube.com/vi/"+imgID+"/maxresdefault.jpg"
	else:
		imgSrc = "/static/defaultThumbnail.png"	    		

	return {'source':source, 'imgSrc':imgSrc}